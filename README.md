| **Ra-01** | **board** |
| ------ | ------ |
| NSS pin | 10 (4 on ESP32/ESP8266 boards) |
| DIO0 pin | 2 | 
| DIO1 pin | 3 | 

<img src="ra_01_pinout.png">